package ru.t1.semikolenov.tm.command.task;

import ru.t1.semikolenov.tm.api.service.IProjectTaskService;
import ru.t1.semikolenov.tm.api.service.ITaskService;
import ru.t1.semikolenov.tm.command.AbstractCommand;
import ru.t1.semikolenov.tm.enumerated.Role;

public abstract class AbstractTaskCommand extends AbstractCommand {

    public ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    public IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
