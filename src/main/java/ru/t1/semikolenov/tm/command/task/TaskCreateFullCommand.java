package ru.t1.semikolenov.tm.command.task;

import ru.t1.semikolenov.tm.util.TerminalUtil;

import java.util.Date;

public final class TaskCreateFullCommand extends AbstractTaskCommand {

    public static final String NAME = "task-create-full";

    public static final String DESCRIPTION = "Create new task full field.";

    public static final String ARGUMENT = null;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        System.out.println("ENTER DATE BEGIN:");
        final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("ENTER DATE END:");
        final Date dateEnd = TerminalUtil.nextDate();
        final String userId = getUserId();
        getTaskService().create(userId, name, description, dateBegin, dateEnd);
    }

}
