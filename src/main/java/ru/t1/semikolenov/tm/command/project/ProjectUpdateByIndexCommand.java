package ru.t1.semikolenov.tm.command.project;

import ru.t1.semikolenov.tm.util.TerminalUtil;

import java.util.Date;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-update-by-index";

    public static final String DESCRIPTION = "Update project by index.";

    public static final String ARGUMENT = null;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        System.out.println("ENTER DATE BEGIN:");
        final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("ENTER DATE END:");
        final Date dateEnd = TerminalUtil.nextDate();
        final String userId = getUserId();
        getProjectService().updateByIndex(userId, index ,name, description, dateBegin, dateEnd);
    }

}
