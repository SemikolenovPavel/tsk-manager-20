package ru.t1.semikolenov.tm.command.task;

import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.model.Task;
import ru.t1.semikolenov.tm.util.DateUtil;
import ru.t1.semikolenov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-show-by-id";

    public static final String DESCRIPTION = "Display task by id.";

    public static final String ARGUMENT = null;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        final Task task = getTaskService().findOneById(userId, id);
        showTask(task);
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(task.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(task.getDateBegin()));
    }

}
