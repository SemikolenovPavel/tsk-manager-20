package ru.t1.semikolenov.tm.command.project;

import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-complete-by-index";

    public static final String DESCRIPTION = "Complete project by index.";

    public static final String ARGUMENT = null;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        getProjectService().changeProjectStatusByIndex(userId, index, Status.COMPLETED);
    }

}
