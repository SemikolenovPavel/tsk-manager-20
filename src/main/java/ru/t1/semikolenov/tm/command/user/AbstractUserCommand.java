package ru.t1.semikolenov.tm.command.user;

import ru.t1.semikolenov.tm.api.service.IAuthService;
import ru.t1.semikolenov.tm.api.service.IUserService;
import ru.t1.semikolenov.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

}
