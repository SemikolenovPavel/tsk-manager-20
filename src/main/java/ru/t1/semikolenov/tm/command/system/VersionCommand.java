package ru.t1.semikolenov.tm.command.system;

public final class VersionCommand extends AbstractSystemCommand{

    public static final String NAME = "version";

    public static final String DESCRIPTION = "Show application version.";

    public static final String ARGUMENT = "-v";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("1.20.0");
    }

}
