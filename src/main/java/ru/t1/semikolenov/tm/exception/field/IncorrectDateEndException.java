package ru.t1.semikolenov.tm.exception.field;

public final class IncorrectDateEndException extends AbstractFieldException {

    public IncorrectDateEndException() {
        super("Error! Data end is incorrect...");
    }

}
