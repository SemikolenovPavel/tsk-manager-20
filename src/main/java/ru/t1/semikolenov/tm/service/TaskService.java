package ru.t1.semikolenov.tm.service;

import ru.t1.semikolenov.tm.api.repository.ITaskRepository;
import ru.t1.semikolenov.tm.api.service.ITaskService;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.exception.entity.TaskNotFoundException;
import ru.t1.semikolenov.tm.exception.field.*;
import ru.t1.semikolenov.tm.model.Task;

import java.util.*;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    public Task create(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.create(userId, name);
    }

    @Override
    public Task create(final String userId, final String name, String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        return repository.create(userId, name, description);
    }

    @Override
    public Task create(final String userId, String name, String description, Date dateBegin, Date dateEnd) {
        final Task task = repository.create(name, description);
        if (task == null) throw new TaskNotFoundException();
        if (dateBegin == null) throw new IncorrectDateBeginException();
        else task.setDateBegin(dateBegin);
        if (dateEnd == null) throw new IncorrectDateEndException();
        else task.setDateEnd(dateEnd);
        task.setUserId(userId);
        return task;
    }

    @Override
    public Task updateById(final String userId, final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return task;
    }

    @Override
    public Task updateById(final String userId, final String id, final String name, final String description, Date dateBegin, Date dateEnd) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        task.setName(name);
        task.setDescription(description);
        if (dateBegin == null) throw new IncorrectDateBeginException();
        else task.setDateBegin(dateBegin);
        if (dateEnd == null) throw new IncorrectDateEndException();
        else task.setDateEnd(dateEnd);
        task.setUserId(userId);
        return task;
    }

    @Override
    public Task updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return task;
    }

    @Override
    public Task updateByIndex(final String userId, final Integer index, final String name, final String description, Date dateBegin, Date dateEnd) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        if (dateBegin == null) throw new IncorrectDateBeginException();
        else task.setDateBegin(dateBegin);
        if (dateEnd == null) throw new IncorrectDateEndException();
        else task.setDateEnd(dateEnd);
        task.setUserId(userId);
        return task;
    }

    @Override
    public Task changeStatusById(final String userId, final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        task.setUserId(userId);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final String userId, final Integer index, final Status status) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        task.setUserId(userId);
        return task;
    }

}
