package ru.t1.semikolenov.tm.api.repository;

import ru.t1.semikolenov.tm.model.Project;
public interface IProjectRepository extends IUserOwnedRepository<Project>{

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

}
