package ru.t1.semikolenov.tm.api.repository;

import ru.t1.semikolenov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task>{

    List<Task> findAllByProjectId(String userId, String projectId);

    Task create(String userId, String name);

    Task create(String userId, String name, String description);

}
