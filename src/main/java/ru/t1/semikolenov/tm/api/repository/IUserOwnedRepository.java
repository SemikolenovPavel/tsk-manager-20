package ru.t1.semikolenov.tm.api.repository;

import ru.t1.semikolenov.tm.enumerated.Sort;
import ru.t1.semikolenov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(String userId);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    List<M> findAll(String userId, Sort sort);

    M add(String userId, M model);

    boolean existsById(String userId, String id);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    int getSize(String userId);

    M remove(String userId, M model);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

}
