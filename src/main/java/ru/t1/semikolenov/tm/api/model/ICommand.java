package ru.t1.semikolenov.tm.api.model;

public interface ICommand {

    String getName();

    String getDescription();

    String getArgument();

}
